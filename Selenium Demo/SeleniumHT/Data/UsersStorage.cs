﻿using SeleniumHT.Entities;

namespace SeleniumHT.Data
{
    public class UsersStorage
    {
        public static UserEntity AdminUser => new UserEntity { UserName = "Onime-power", Password = "12345Kostia" };
        public static UserEntity UserWithNotValidCredentials => new UserEntity { UserName = "RandomText76", Password = "RandomPassword87643123" };
    }
}

﻿using OpenQA.Selenium;
using System;
using System.Threading;

namespace SeleniumHT.Common
{
    public class WaiterHelper
    {
        private IWebDriver _driver;
        public WaiterHelper(IWebDriver driver)
        {
            _driver = driver;
        }

        public void WaitUntilAlertIsPresent()
        {
            int tries = 3;
            int tryNumber = 0;

            while (tryNumber < tries)
            {
                try
                {
                    _driver.SwitchTo().Alert();
                    break;
                }
                catch (Exception)
                {
                    Console.WriteLine("No alert is present");
                    tryNumber++;
                    Thread.Sleep(TimeSpan.FromSeconds(3));
                }
            }
            
        }

    }
}

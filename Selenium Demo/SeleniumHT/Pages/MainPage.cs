﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using SeleniumHT.Entities;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace SeleniumHT.Pages
{
    public class MainPage
    {
        private IWebDriver _driver;
        private WebDriverWait _wait;
        private TimeSpan defaultTimeout = TimeSpan.FromSeconds(10);

        public MainPage(IWebDriver driver)
        {
            _driver = driver;
            _wait = new WebDriverWait(_driver, defaultTimeout);
        }

        public IWebElement LogInButton => _driver.FindElement(By.Id("login2"));
        public IWebElement UserNameInput => _driver.FindElement(By.Id("loginusername"));
        public IWebElement PasswordInput => _driver.FindElement(By.Id("loginpassword"));
        public IWebElement SubmitLoginButton => _driver.FindElement(By.XPath("//button[text() = 'Log in']"));

        public By CategoriesLocator => By.XPath("//div[@class='list-group']//a");
        public By ItemsLocator => By.XPath("//div[@id='tbodyid']/div");
        public By LogoutLocator => By.Id("logout2");
        public By WelcomeUserLocator(string userName) => By.XPath($"//a[text() = 'Welcome {userName}']");


        public MainPage LogIn(UserEntity user)
        {
            LogInButton.Click();
            UserNameInput.SendKeys(user.UserName);
            PasswordInput.SendKeys(user.Password);
            SubmitLoginButton.Click();
            _wait.Until(ExpectedConditions.ElementIsVisible(LogoutLocator));
            return this;
        }

        public MainPage OpenCategory(string categoryName)
        {
            ReadOnlyCollection<IWebElement> categories = _driver.FindElements(CategoriesLocator);
            IWebElement category = categories.Single(cat => cat.Text.Equals(categoryName));
            category.Click();
            return this;
        }

        public MainPage OpenItem(string itemName)
        {
            var items = _driver.FindElements(ItemsLocator);
            var item = items.Single(itm => itm.Text.Equals(itemName));
            item.Click();
            return this;
        }
    }
}

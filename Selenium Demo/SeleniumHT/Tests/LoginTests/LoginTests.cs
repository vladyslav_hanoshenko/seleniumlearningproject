﻿using FluentAssertions;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Support.UI;
using SeleniumHT.Common;
using SeleniumHT.Data;
using SeleniumHT.Pages;
using System;

namespace SeleniumHT.Tests.LoginTests
{
    [TestFixture]
    public class LoginTests
    {
        private IWebDriver driver;
        private WaiterHelper waiterHelper;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.demoblaze.com/");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            waiterHelper = new WaiterHelper(driver);
        }

        [Test]
        public void User_Should_Be_Successfully_Logged_In_With_Valid_Credentials()
        {
            MainPage mainPage = new MainPage(driver);
            mainPage.LogIn(UsersStorage.AdminUser);

            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(mainPage.LogoutLocator));
            new WebDriverWait(driver, TimeSpan.FromSeconds(10)).Until(ExpectedConditions.ElementIsVisible(mainPage.WelcomeUserLocator(UsersStorage.AdminUser.UserName)));
        }

        [Test]
        public void User_Should_Not_Be_Logged_In_With_InValid_Credentials()
        {
            MainPage mainPage = new MainPage(driver);
            mainPage.LogIn(UsersStorage.UserWithNotValidCredentials);
            waiterHelper.WaitUntilAlertIsPresent();

            string expectedAlertMessage = "User does not exist.";
            string actualAlertMessage = driver.SwitchTo().Alert().Text;

            actualAlertMessage.Should().BeEquivalentTo(expectedAlertMessage);
        }

        [TearDown]
        public void TearDown()
        {
            driver.Quit();
        }
    }
}

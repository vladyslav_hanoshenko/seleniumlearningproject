﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SeleniumHT.Common;
using SeleniumHT.Data;
using SeleniumHT.Pages;
using System;
using System.Collections.Generic;
using System.Text;

namespace SeleniumHT.Tests.OrderTests
{
    [TestFixture]
    public class OrderTests
    {
        private IWebDriver driver;
        private WaiterHelper waiterHelper;

        [SetUp]
        public void Setup()
        {
            driver = new ChromeDriver();
            driver.Navigate().GoToUrl("https://www.demoblaze.com/");
            driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(5);
            waiterHelper = new WaiterHelper(driver);
        }

        [Test]
        public void User_Should_Be_Able_To_Make_An_Order_With_Valid_Data()
        {
            MainPage mainPage = new MainPage(driver);
            
            mainPage
                .LogIn(UsersStorage.AdminUser)
                .OpenCategory("Laptops")
                .OpenItem("2017 Dell 15.6 Inch");
        }

    }
}
